Pod::Spec.new do |s|

  s.name         = "Mob2allFramework"
  s.version      = "1.0"
  s.summary      = "Mob2allFramework."

  s.description  = <<-DESC
                   MOB2all framework
                   DESC

  s.homepage     = "http://mob2all.com.br"

   s.license      = { :type => "MIT", :text => "LICENSE"}

  
  s.author             = { "Lucas Correa" => "contato@lucascorrea.com" }
  s.platform     = :ios, "7.0"
  s.ios.deployment_target = "7.0"


  s.source       = { :git => "git@bitbucket.org:teammob2allapp/mob2allframeworkiospublic.git", :tag => "1.0" }
  
  s.resources = "MOB2allFrameworkPublic/Mob2all.bundle"
  
  s.frameworks = "Security", "OpenGLES", "MobileCoreServices", "CoreVideo", "CoreMedia", "AudioToolbox", "MessageUI", "AVFoundation", "CoreText"
  s.vendored_frameworks = 'MOB2allFrameworkPublic/Mob2all.framework'
  s.libraries = "sqlite3" , "iconv"


  s.requires_arc = true

  s.dependency "AFNetworking"
  s.dependency "CardIO"

end
