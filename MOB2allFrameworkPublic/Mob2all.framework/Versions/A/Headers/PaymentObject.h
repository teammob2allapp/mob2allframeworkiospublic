//
//  PaymentObject.h
//  Mob2all
//
//  Created by Lucas Correa on 26/12/13.
//  Copyright (c) 2013 NTK Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, Mob2allTheme){
    Mob2allThemeDefault = 0,
    Mob2allThemeBlue = 1,
    Mob2allThemeRed = 2,
    Mob2allThemeGreen = 3,
    Mob2allThemeOrange = 4
};

@interface PaymentObject : NSObject

@property (assign, nonatomic) NSUInteger userId;
@property (copy, nonatomic) NSString *token;
@property (copy, nonatomic) NSString *document;
@property (copy, nonatomic) NSString *key;
@property (copy, nonatomic) NSString *frameworkToken;
@property (assign, nonatomic) Mob2allTheme mob2alTheme;
@property (assign, nonatomic) NSUInteger emporiumId;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
@property (assign, getter = isReceipt) BOOL receipt;
@property (assign, getter = isInvoice) BOOL invoice;


- (id)initWithUserId:(NSUInteger)userId key:(NSString *)key emporiumId:(NSUInteger)emporiumId token:(NSString *)token frameworkToken:(NSString *)frameworkToken theme:(Mob2allTheme)theme;

- (id)initWithUserId:(NSUInteger)userId key:(NSString *)key emporiumId:(NSUInteger)emporiumId token:(NSString *)token latitude:(double)latitude longitude:(double)longitude receipt:(BOOL)receipt invoice:(BOOL)invoice frameworkToken:(NSString *)frameworkToken theme:(Mob2allTheme)theme;


@end
