//
//  CardRegisterViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 22/10/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Field.h"
#import "Card.h"
#import "CardIO.h"
#import "constants.h"

#import "CardIOPaymentViewControllerDelegate.h"

@interface CardRegisterViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet Field *numberText;
@property (weak, nonatomic) IBOutlet UITextField *validateText;
@property (weak, nonatomic) IBOutlet UITextField *cvvText;
@property (weak, nonatomic) IBOutlet UILabel *flagLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (strong, nonatomic) void (^didRegisterCard)(Card *card);
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) NSMutableArray *cardsGranted;

- (IBAction)cvvButtonAction:(id)sender;
- (IBAction)registerButtonAction:(id)sender;
- (IBAction)flagButtonAction:(id)sender;

@end
