//
//  LoadingView.h
//  
//
//  Created by xiss burg on 6/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (nonatomic, strong) UILabel *loadingLabel;

- (void)showOnView:(UIView *)view animated:(BOOL)animated;
- (void)showOnView:(UIView *)view frame:(CGRect)frame animated:(BOOL)animated;
- (void)showOnView:(UIView *)view frame:(CGRect)frame belowSubview:(UIView *)subview animated:(BOOL)animated;
- (void)hideAnimated:(BOOL)animated;

@end
