//
//  ReceiptViewController.h
//  MobilePayment
//
//  Created by Lucas Correa on 04/07/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Receipt.h"
#import "constants.h"

#import <MessageUI/MessageUI.h>


@interface ReceiptViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *legendaLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardLegLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLegLabel;
@property (weak, nonatomic) IBOutlet UILabel *autorizeLegLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *autorizeNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLegLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (copy, nonatomic) NSString *productName;
@property (strong, nonatomic) Receipt *receipt;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

@end
