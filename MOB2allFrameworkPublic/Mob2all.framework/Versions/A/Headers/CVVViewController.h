//
//  CVVViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 10/06/13.
//  Copyright (c) 2013 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVVViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *background;
@end
