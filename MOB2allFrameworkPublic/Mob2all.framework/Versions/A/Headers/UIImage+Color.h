//
//  UIImage+Color.h
//
//  Created by Sam McEwan sammcewan@me.com on 15/10/12.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

- (UIImage*)changeColor:(UIColor*)color;
- (UIImage *)replaceColor:(UIColor *)color withTolerance:(float)tolerance;
- (UIImage *)replaceColor:(UIColor *)color withTolerance:(float)tolerance navigation:(BOOL)navi;

@end
