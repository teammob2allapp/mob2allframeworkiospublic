//
//  BalanceObject.h
//  Mob2all
//
//  Created by Lucas Correa on 20/01/15.
//  Copyright (c) 2015 NTK Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentObject.h"

@interface BalanceObject : NSObject

@property (assign, nonatomic) NSUInteger userId;
@property (copy, nonatomic) NSString *token;
@property (copy, nonatomic) NSString *document;
@property (copy, nonatomic) NSString *key;
@property (copy, nonatomic) NSString *frameworkToken;
@property (assign, nonatomic) Mob2allTheme mob2alTheme;


- (id)initWithUserId:(NSUInteger)userId key:(NSString *)key token:(NSString *)token frameworkToken:(NSString *)frameworkToken theme:(Mob2allTheme)theme;


@end
