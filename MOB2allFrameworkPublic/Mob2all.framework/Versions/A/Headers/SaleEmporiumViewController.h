//
//  SaleEmporiumViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 16/10/13.
//  Copyright (c) 2013 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BalancePoint.h"


@interface SaleEmporiumViewController : UIViewController

@property (strong, nonatomic) BalancePoint *balance;

@end
