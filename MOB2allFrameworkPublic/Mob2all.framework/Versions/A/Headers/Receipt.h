//
//  Receipt.h
//  Mob2all
//
//  Created by Lucas Correa on 23/01/13.
//  Copyright (c) 2013 Siriuscode Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    CreditType,
    PointType
}TransitionType;

@interface Receipt : NSObject


@property (assign, getter = isPaidCard) BOOL paidCard;
@property (copy, nonatomic) NSString *ccm;
@property (copy, nonatomic) NSString *cnpj;
@property (copy, nonatomic) NSString *ie;
@property (copy, nonatomic) NSString *codAutorization;
@property (copy, nonatomic) NSString *productDetail;
@property (copy, nonatomic) NSString *street;
@property (assign, nonatomic) NSUInteger transitionId;
@property (copy, nonatomic) NSString *emporiumName;
@property (copy, nonatomic) NSString *gatewayName;
@property (copy, nonatomic) NSString *productName;
@property (copy, nonatomic) NSString *numberRps;
@property (copy, nonatomic) NSString *numberTicket;
@property (copy, nonatomic) NSString *operatorName;
@property (copy, nonatomic) NSString *periodTotal;
@property (copy, nonatomic) NSString *outputUp;
@property (copy, nonatomic) NSString *serie;
@property (copy, nonatomic) NSString *tax;
@property (copy, nonatomic) NSString *tid;
@property (copy, nonatomic) NSString *total;
@property (copy, nonatomic) NSString *pricePay;
@property (copy, nonatomic) NSString *pricePaid;
@property (copy, nonatomic) NSString *datePayment;
@property (copy, nonatomic) NSString *hourPayment;
@property (copy, nonatomic) NSString *hourEntry;
@property (assign, nonatomic) NSUInteger userId;
@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *numberCard;
@property (assign, nonatomic) TransitionType type;

- (id)initWithDictionary:(NSDictionary *)dic transitionType:(TransitionType)type;

@end
