//
//  PromotionObject.h
//  Mob2all
//
//  Created by Lucas Correa on 15/01/15.
//  Copyright (c) 2015 NTK Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PaymentObject.h"

@interface PromotionObject : NSObject

@property (assign, nonatomic) NSUInteger userId;
@property (copy, nonatomic) NSString *token;
@property (copy, nonatomic) NSString *document;
@property (copy, nonatomic) NSString *key;
@property (copy, nonatomic) NSString *frameworkToken;
@property (assign, nonatomic) Mob2allTheme mob2alTheme;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
@property (assign, getter = isReceipt) BOOL receipt;
@property (assign, getter = isInvoice) BOOL invoice;


- (id)initWithUserId:(NSUInteger)userId key:(NSString *)key token:(NSString *)token latitude:(double)latitude longitude:(double)longitude frameworkToken:(NSString *)frameworkToken theme:(Mob2allTheme)theme;


@end
