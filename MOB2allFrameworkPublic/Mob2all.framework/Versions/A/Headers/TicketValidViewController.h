//
//  TIcketValidViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 10/06/13.
//  Copyright (c) 2013 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketValidViewController : UIViewController

- (void)showForDictionary:(NSDictionary *)dic;

- (IBAction)closeButtonAction:(id)sender;

@end
