//
//  BalanceStatementViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 05/11/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BalanceStatementViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *pointLabel;

@end
