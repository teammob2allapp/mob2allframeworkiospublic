//
//  ThemeManager.h
//  Mob2all
//
//  Created by Lucas Correa on 23/07/14.
//  Copyright (c) 2014 NTK Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mob2allPayment.h"
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ThemeFieldType) {
    LabelNavigationTextColor = 0,
    LabelTextColor = 1,
    ButtonBackgroundColor = 2,
    TitleViewBackgroundColor = 3,
    ViewBackgroundColor = 4,
    ArrowColor = 5,
    SubTitleLabelTextColor = 6,
    SubTitleViewBackgroundColor = 7,
    CellFirstBackgroundColor = 8,
    CellSecondBackgroundColor = 9,
    ButtonAddBackgroundColor = 10,
    LabelTitleTextColor = 11,
    ComboViewBackgroundColor = 12,
    TextFieldTextColor = 13,
    ButtonPopupBackgroundColor = 14,
    LabelPopupTextColor = 15,
    ButtonNoBackgroundColor = 16,
    LabelNoTextColor = 17,
    ButtonYesBackgroundColor = 18,
    LabelYesTextColor = 19,
    ProgressBarDefaultColor = 20,
    ProgressBarProgressColor = 21
};

@interface ThemeManager : NSObject

+ (void)applyTheme:(Mob2allTheme)theme;
+ (UIColor *)getColorBy:(ThemeFieldType)themeType;

@end
