//
//  Mob2allPayment.h
//  Mob2all
//
//  Created by Lucas Correa on 18/12/13.
//  Copyright (c) 2013 NTK Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "PaymentObject.h"
#import "PromotionObject.h"
#import "BalanceObject.h"
#import "User.h"
#import "Receipt.h"
#import "ThemeManager.h"

typedef NS_ENUM(NSUInteger, Sex){
    Male,
    Female
};

@protocol Mob2allPaymentDelegate <NSObject>

@required
- (void)mob2allPaymentDidSuccessReceipt:(Receipt *)receipt;
- (void)mob2allPaymentDidError:(NSError *)error;
- (void)mob2allPaymentDidCanceled;

@end

@interface Mob2allPayment : NSObject

@property (strong, nonatomic) id <Mob2allPaymentDelegate> delegate;

+ (Mob2allPayment *)sharedInstance;
+ (NSBundle *)frameworkBundle;

+ (PaymentObject *)paymentObject;
+ (PromotionObject *)promotionObject;
+ (BalanceObject *)balanceObject;

+ (void)registerOrLoginUsername:(NSString *)username lastName:(NSString *)lastName document:(NSString *)cpf email:(NSString *)email phone:(NSString *)phone sex:(Sex)sex birth:(NSString *)birth emporiumId:(NSInteger)emporiumId completion:(void (^)(User *user))completion failure:(void (^)(NSError *error))failure;

+ (void)paymentShowViewController:(UIViewController *)viewController frameworkToken:(NSString *)frameworkToken userId:(NSUInteger)userId key:(NSString *)key token:(NSString *)token emporiumId:(NSUInteger)emporiumId latitude:(double)latitude longitude:(double)longitude receipt:(BOOL)receipt invoice:(BOOL)invoice theme:(Mob2allTheme)theme;

+ (void)promotionsShowViewController:(UIViewController *)viewController frameworkToken:(NSString *)frameworkToken userId:(NSUInteger)userId key:(NSString *)key token:(NSString *)token latitude:(double)latitude longitude:(double)longitude theme:(Mob2allTheme)theme;

+ (void)balanceShowViewController:(UIViewController *)viewController frameworkToken:(NSString *)frameworkToken userId:(NSUInteger)userId key:(NSString *)key token:(NSString *)token theme:(Mob2allTheme)theme;

+ (void)awardShowViewController:(UIViewController *)viewController frameworkToken:(NSString *)frameworkToken userId:(NSUInteger)userId key:(NSString *)key token:(NSString *)token latitude:(double)latitude longitude:(double)longitude theme:(Mob2allTheme)theme;

+ (void)reportShowViewController:(UIViewController *)viewController frameworkToken:(NSString *)frameworkToken userId:(NSUInteger)userId key:(NSString *)key token:(NSString *)token emporiumId:(NSUInteger)emporiumId theme:(Mob2allTheme)theme;


- (NSString *)getVersionFramework;

@end
