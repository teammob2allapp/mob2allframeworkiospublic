//
//  User.h
//  Mob2all
//
//  Created by Lucas Correa on 14/01/13.
//  Copyright (c) 2013 Siriuscode Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *lastname;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *pass;
@property (copy, nonatomic) NSURL *photoUrl;
@property (assign, nonatomic) NSInteger userId;
@property (copy, nonatomic) NSString *document;
@property (copy, nonatomic) NSString *key;
@property (copy, nonatomic) NSString *shareLocation;
@property (copy, nonatomic) NSString *sex;
@property (copy, nonatomic) NSString *birthday;
@property (copy, nonatomic) NSString *phone;
@property (copy, nonatomic) NSString *photo;
@property (copy, nonatomic) NSString *sale;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;
@property (copy, nonatomic) NSString *token;
@property (assign, nonatomic) BOOL isUserFacebook;
@property (assign, nonatomic) BOOL isTermAccept;
@property (copy, nonatomic) NSString *termVersion;

- (id)initWithDictionary:(NSDictionary *)dic;
- (NSString *)latitudeString;
- (NSString *)longitudeString;

@end
