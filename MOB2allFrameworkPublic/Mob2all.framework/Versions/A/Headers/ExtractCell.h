//
//  ExtractCell.h
//  Mob2all
//
//  Created by Lucas Correa on 06/02/13.
//  Copyright (c) 2013 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExtractCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
