//
//  TransactionReportViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 05/02/13.
//  Copyright (c) 2013 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentObject.h"


@interface TransactionReportViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) PaymentObject *paymentObject;


- (IBAction)monthTouchUpInside:(id)sender;
- (IBAction)yearTouchUpInside:(id)sender;
- (IBAction)leftButtonAction:(id)sender;
- (IBAction)rightButtonAction:(id)sender;

@end
