//
//  CheckPriceParkingViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 12/10/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WSPayment.h"
//#import "PasscodeViewController.h"

@interface CheckPriceParkingViewController : UIViewController <UIAlertViewDelegate, UITextFieldDelegate, UIActionSheetDelegate>
//,PasscodeViewControllerDelegate>


@property (weak, nonatomic) IBOutlet UILabel *shoppingLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeEntryLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTotalLabel;
@property (strong, nonatomic) Payment *payment;
@property (assign, nonatomic) NSInteger typeCapture;
@property (weak, nonatomic) IBOutlet UILabel *pricePayedLabel;
@property (weak, nonatomic) IBOutlet UILabel *pricePayedLegLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeEntryLegLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLegLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceTotalLegLabel;
@property (weak, nonatomic) IBOutlet UIImageView *separatorImageView;
@property (weak, nonatomic) IBOutlet UILabel *dateEntryLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateEntryLegLabel;

@property (weak, nonatomic) IBOutlet UILabel *numberCardLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flagImageView;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (weak, nonatomic) IBOutlet UILabel *shoppingNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLegLabel;
@property (weak, nonatomic) IBOutlet UIImageView *totalSeparadorImageView;
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;


- (IBAction)chooseCardButtonAction:(id)sender;
- (IBAction)addCardOrPaymentConfirmButtonAction:(id)sender;

@end
