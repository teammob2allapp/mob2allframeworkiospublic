//
//  PUtils.m
//  MobilePayment
//
//  Created by Lucas Correa on 08/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "constants.h"

@interface Utils : NSObject

NSString* convertDistanceToString(float distance);

+ (void)encodeMyObject:(id)object forKey:(NSString*)key;
+ (id)decodeMyObjectForKey:(NSString*)key;

@end
