//
//  QRCodeReader.h
//  Mob2all
//
//  Created by Lucas Correa on 09/09/13.
//  Copyright (c) 2013 Siriuscode Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZBarReaderViewController.h"
#import "constants.h"

@interface QRCodeReader : NSObject <ZBarReaderDelegate>

@property (strong, nonatomic) UIViewController *viewController;
@property (strong, nonatomic) void (^didQRCodeRead) (NSString *qrcode);
@property (strong, nonatomic) void (^didQRCodeCanceled) (void);


+ (void)qrcodeShowInViewController:(UIViewController *)viewController didQRCodeRead:(void (^) (NSString *qrcode))completion;

@end
