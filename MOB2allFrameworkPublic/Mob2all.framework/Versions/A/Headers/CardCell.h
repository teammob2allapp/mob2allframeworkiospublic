//
//  CardCell.h
//  MobilePayment
//
//  Created by Lucas Correa on 09/06/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CardCellDelegate <NSObject>

- (void)cardCellDeleteCell:(UITableViewCell *)cell;

@end

@interface CardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cardLabel;
@property (weak, nonatomic) IBOutlet UIImageView *cardFlag;
@property (weak, nonatomic) id<CardCellDelegate>delegate;

- (IBAction)deleteCardButtonAction:(id)sender;

@end
