//
//  QRCodeViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 11/10/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import "Field.h"
#import "PaymentObject.h"

@interface QRCodeViewController : UIViewController <ZBarReaderDelegate, UITextFieldDelegate>

@property (strong, nonatomic) PaymentObject *paymentObject;

- (IBAction)scanQRCodeBarButtonAction:(id)sender;

@end
