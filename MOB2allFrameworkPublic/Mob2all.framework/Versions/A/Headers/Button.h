//
//  Button.h
//  Mob2all
//
//  Created by Lucas Correa on 27/12/13.
//  Copyright (c) 2013 NTK Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Button : UIButton

typedef enum {
    ButtonStyleCustom = 0,
    ButtonStyleYes,
    ButtonStyleNo
} ButtonStyle;

@property (nonatomic, assign) ButtonStyle type;

- (id)initWithFrame:(CGRect)frame andType:(ButtonStyle)type;


@end
