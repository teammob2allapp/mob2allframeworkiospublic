//
//  ItemPickerView.h
//  MobilePayment
//
//  Created by Lucas Correa on 08/06/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"

@interface ItemPickerView : UIView <UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *itemPickerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *itemDatePickerView;
@property (weak, nonatomic) IBOutlet UIView *itemView;
@property (strong, nonatomic) NSMutableArray *itemArray;
@property (copy, nonatomic) void (^didPickerItem) (NSString *item);
@property (copy, nonatomic) NSString *itemSelected;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (assign, nonatomic) BOOL isDatePicker;
@property (assign, nonatomic) BOOL isCardPicker;


- (void)showInView:(UIView *)view;
- (void)showInView:(UIView *)view title:(NSString *)title;
- (void)showCardInView:(UIView *)view title:(NSString *)title;
- (void)datePickerShowInView:(UIView *)view title:(NSString *)title;
- (void)close;

- (IBAction)chooseButtonAction:(id)sender;

@end
