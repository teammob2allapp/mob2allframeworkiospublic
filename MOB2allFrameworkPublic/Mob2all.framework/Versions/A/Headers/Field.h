//
//  Field.h
//  MobilePayment
//
//  Created by Lucas Correa on 27/06/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

enum FieldType{
    Default = 0,
    Numeric = 1,
    Character = 2,
    Text = 3,
    NumericNotReturn = 4,
    NumericOK = 5
};

@interface Field : UITextField

@property (nonatomic, assign) int max;
@property (nonatomic, retain) NSString* mask;
@property (nonatomic, assign) enum FieldType type;

@end
