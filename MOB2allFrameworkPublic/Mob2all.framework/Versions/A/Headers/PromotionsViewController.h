//
//  PromotionsViewController.h
//  Mob2all
//
//  Created by Lucas Correa on 05/11/12.
//  Copyright (c) 2012 Siriuscode Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Field.h"
#import "PromotionCell.h"
#import "PromotionDetailViewController.h"
#import "ZBarReaderViewController.h"
#import "constants.h"

@interface PromotionsViewController : UIViewController <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, PromotionCellDelegate, ZBarReaderDelegate>

@property (weak, nonatomic) IBOutlet Field *searchText;
@property (weak, nonatomic) IBOutlet UITableView *promotionsTableView;
@property (weak, nonatomic) IBOutlet UIButton *qrcodeButton;

- (IBAction)qrcodeButtonAction:(id)sender;

@end
