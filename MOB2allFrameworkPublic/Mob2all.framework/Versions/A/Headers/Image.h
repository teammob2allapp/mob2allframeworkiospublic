//
//  Image.h
//  Mob2all
//
//  Created by Lucas Correa on 27/12/13.
//  Copyright (c) 2013 NTK Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Image : UIImage

+ (UIImage *)imageNamed:(NSString *)name;

@end
